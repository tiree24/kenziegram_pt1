const express = require("express");
const path = "./public/uploads";
const multer = require("multer");
const app = express();
// const ejs = require("ejs");
const upload = multer({ dest: "./public/uploads" });
const fs = require("fs");
const port = 3000;

// app.set("view engine", "ejs");

app.use(express.static("./public"));

app.get("/", (req, res) => {
  fs.readdir(path, function (err, items) {
    let imagesTag = items.map((item) => {
      return `<img src= " uploads/${item}" /> `;
    });

    console.log(items);

    res.send(`<h1>Welcome to Kenziegram!</h1> <form action="/upload" enctype="multipart/form-data" method="POST"> 
    <input type="file" name="myFile" />
    <input type="submit" value="Upload a file"/>
 </form>
    ${imagesTag}`);
  });
});

app.post("/upload", upload.single("myFile"), (req, res) => {
  const file = req.file.filename;
  if (!file) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  }
  res.send(`<a href= "/"> back home</a> <img src= "/uploads/${file}"/>`);
});

// app.get("/", (req, res) => res.send("index"));

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));
